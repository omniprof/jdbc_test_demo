-- Best practice MySQL
--
-- This script needs to run only once

DROP DATABASE IF EXISTS GAMER_DB;
CREATE DATABASE GAMER_DB;

USE GAMER_DB;

DROP USER IF EXISTS TheUser@localhost;
CREATE USER TheUser@'localhost' IDENTIFIED BY 'pancake';

GRANT ALL ON GAMER_DB.* TO TheUser@'localhost';

FLUSH PRIVILEGES;

