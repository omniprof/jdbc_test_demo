/**
 * Basic JDBC routines using unit testing
 *
 * @author Ken Fogel
 * @version 2.2
 */
package com.jdbc_test_demo.persistence;

import com.jdbc_test_demo.entities.Gamer;
import com.jdbc_test_demo.entities.Games;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the GamingDAO interface
 *
 * Exceptions are possible whenever a JDBC object is used. When these exceptions
 * occur it should result in some message appearing to the user and possibly
 * some corrective action. To simplify this, all exceptions are thrown and not
 * caught here. The methods that you write to call any of these methods must
 * either use a try/catch or continue throwing the exception.
 *
 * Added logging of exceptions in the catch before re-throwing the exception
 *
 */
public class GamingDAOImpl implements GamingDAO {

    private final static Logger LOG = LoggerFactory.getLogger(GamingDAOImpl.class);

    private final String url = "jdbc:mysql://localhost:3306/GAMER_DB?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String user = "TheUser";
    private final String password = "pancake";

    /**
     * Default constructor
     */
    public GamingDAOImpl() {
        super();
    }

    /**
     * Retrieve all the gamer records and add the games of each gamer
     *
     * @return ArrayList of Gamer objects
     * @throws SQLException
     */
    @Override
    public List<Gamer> findAll() throws SQLException {
        List<Gamer> rows = new ArrayList<>();

        String selectQuery = "SELECT GAMERID,LASTNAME,FIRSTNAME,GAMERHANDLE,MEMBERSHIPDATE FROM GAMER";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);
                ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                Gamer gamer = makeGamer(resultSet);
                findAllGames(gamer);
                rows.add(gamer);
            }
        } catch (SQLException ex) {
            LOG.debug("findAll error", ex.getMessage());
            throw ex;
        }
        return rows;
    }

    /**
     * This method exists to demonstrate the relationship between gamers and
     * games by display each gamer and their games.
     *
     * @param gamer The Gamer object to which the Games must be added to the
     * ArrayList.
     * @throws SQLException
     */
    private void findAllGames(Gamer gamer) throws SQLException {
        String selectQuery = "SELECT ID,GAMERID, DATEOFPURCHASE, GAMETITLE, PLATFORM FROM GAMES WHERE GAMERID = ?";

        try (Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(selectQuery)) {
            pStatement.setInt(1, gamer.getGamerID());
            try (ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    gamer.getListOfGames().add(makeGames(resultSet));
                }
            }
        } catch (SQLException ex) {
            LOG.debug("findAllGames error", ex.getMessage());
            throw ex;
        }
    }

    /**
     * Create a Gamer record
     *
     * Added try-with-resources when retrieving the primary key
     *
     * @param gamer
     * @return number of records created, should be 0 or 1
     * @throws SQLException
     */
    @Override
    public int create(Gamer gamer) throws SQLException {
        int records;

        String sql = "INSERT INTO GAMER (LASTNAME, FIRSTNAME, GAMERHANDLE, MEMBERSHIPDATE) values (?, ?, ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            pStatement.setString(1, gamer.getLastName());
            pStatement.setString(2, gamer.getFirstName());
            pStatement.setString(3, gamer.getGamerHandle());
            pStatement.setTimestamp(4, gamer.getMembershipDate());
            records = pStatement.executeUpdate();
            try (ResultSet rs = pStatement.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                gamer.setGamerID(recordNum);
                LOG.debug("New record ID is " + recordNum);
            }
        } catch (SQLException ex) {
            LOG.debug("create error", ex.getMessage());
            throw ex;
        }
        return records;
    }

    @Override
    public Gamer findID(int id) throws SQLException {
        Gamer gamer = new Gamer();
        String selectQuery = "SELECT GAMERID,LASTNAME,FIRSTNAME,GAMERHANDLE,MEMBERSHIPDATE FROM GAMER WHERE GAMERID = ?";
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, id);

            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    gamer = makeGamer(resultSet);
                    findAllGames(gamer);
                }
            }
        } catch (SQLException ex) {
            LOG.debug("findID error", ex.getMessage());
            throw ex;
        }
        return gamer;
    }

    // The following methods are todo.
    // Create Game
    @Override
    public int create(Games game) throws SQLException {
        return 0;
    }

    // Update Gamer
    @Override
    public int update(Gamer gamer) throws SQLException {
        return 0;
    }

    // Update Game
    @Override
    public int update(Games games) throws SQLException {
        return 0;
    }

    // Delete Gamer
    @Override
    public int deleteGamer(int ID) throws SQLException {
        return 0;
    }

    // Delete game
    @Override
    public int deleteGames(int ID) throws SQLException {
        return 0;
    }

    /**
     * Create a Gamer object from the current row in the ResultSet
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    private Gamer makeGamer(ResultSet rs) throws SQLException {
        Gamer gamer = new Gamer();
        gamer.setLastName(rs.getString("LASTNAME"));
        gamer.setFirstName(rs.getString("FIRSTNAME"));
        gamer.setGamerHandle(rs.getString("GAMERHANDLE"));
        gamer.setMembershipDate(rs.getTimestamp("MEMBERSHIPDATE"));
        gamer.setGamerID(rs.getInt("GAMERID"));
        return gamer;
    }

    /**
     * Create a Games object from the current row in the ResultSet
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    private Games makeGames(ResultSet rs) throws SQLException {
        Games games = new Games();
        games.setGamerId(rs.getInt("GAMERID"));
        games.setGameTitle(rs.getString("GAMETITLE"));
        games.setDateOfPurchase(rs.getTimestamp("DATEOFPURCHASE"));
        games.setPlatform(rs.getString("PLATFORM"));
        games.setId(rs.getInt("ID"));
        return games;
    }
}
